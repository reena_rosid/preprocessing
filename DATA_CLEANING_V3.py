
# coding: utf-8

# In[43]:

import json
from bs4 import BeautifulSoup
import pandas as pd
import re
from datetime import datetime


# In[65]:

from nltk.stem import PorterStemmer
port = PorterStemmer()


# In[57]:

# enter the file path
with open('C:\\Users\\reena.manivel\\Downloads\\All2018-08-01.json') as f:
    data = json.load(f)


# In[58]:

# csv read
df=pd.read_csv("C://Users//reena.manivel//Downloads/15_aug_sep.csv")


# In[59]:

len(df)


# In[60]:

#function for removing tags and hex codes
def clean_data_1(text):
    soup = BeautifulSoup(text,"lxml")
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.decompose()    # rip it out
    # get text
    text = soup.get_text()
    txt=text.encode('ascii','replace')
    txt=str(txt).replace("\\n"," ") 
    txt=str(txt).replace("?","")
    text=re.sub(r'^(b\")|^(b\')',"",txt)

    return text


# In[61]:

#loop through each row and get the contents
cnt=[]
i=0
start_time = datetime.now()

for each in df.CONTENT: 
    try:
        
        text=each
        txt=clean_data_1(text)
        cnt.append(txt)
        
    except(TypeError):
        cnt.append('NA')
    i=i+1
    if(i%1000==0):
        print(i)
        
end_time = datetime.now()    
        
print(start_time)
print(end_time)
print('Duration: {}'.format(end_time - start_time))         


# In[48]:

df=pd.DataFrame()

df["CLEANED"]=cnt


# In[62]:

cnt[1:100]


# In[12]:

df.to_excel('C:\\Users\\reena.manivel\\Downloads\\cleaned.xlsx')


# In[63]:

def stemdistuff(text_list): 
    start_time = datetime.now()
    content=[]
    x=0
    for i in range(0,len(text_list)):
        try:
            content_name = [port.stem(m) for m in text_list[i].split()]
            content.append(content_name)
        except(TypeError):
            content.append('NA')
        x = x+1
        if (x%1000==0):
            print('lookup iterator is ',x)
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))
    return content


# In[66]:

stemdistuff(cnt)


# In[ ]:



