import json
from bs4 import BeautifulSoup
import pandas as pd
import re
from datetime import datetime


# enter the file path
with open('C:\\Users\\reena.manivel\\Downloads\\All2018-08-01.json') as f:
    data = json.load(f)
	
# csv read
df=pd.read_csv("C://Users//reena.manivel//Downloads/15_aug_sep.csv")

len(df)

#function for removing tags and hex codes
def clean_data_1(text):
    soup = BeautifulSoup(text,"lxml")
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.decompose()    # rip it out
    # get text
    text = soup.get_text()
    txt=text.encode('ascii','replace')
    txt=str(txt).replace("\\n"," ") 
    txt=str(txt).replace("?","")
    text=re.sub(r'^(b\")|^(b\')',"",txt)

    return text

#loop through each row and get the contents
cnt=[]
i=0
start_time = datetime.now()

for each in df.CONTENT: 
    try:
        
        text=each
        txt=clean_data_1(text)
        cnt.append(txt)
        
    except(TypeError):
        cnt.append('NA')
    i=i+1
    if(i%1000==0):
        print(i)
        
end_time = datetime.now()    
        
print(start_time)
print(end_time)
print('Duration: {}'.format(end_time - start_time))     
	
